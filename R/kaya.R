
#' The Kaya Equation
#'
#' Compute carbon dioxide emissions as a function of populuation, gdp, energy intensity and carbon intensity
#'
#' @param pop Population size in million
#' @param gdp Gross Domestic Product in 1000$/person
#' @param enInt Energy Intensity in Gigajoules/$1000GDP
#' @param carbInt Carbon intensity in tonnes CO2 per GJ
#' @return CO2 in million of tonnes
#' @export
#' @import checkmate
kaya = function(pop, gdp, enInt, carbInt) {
  assert_number(pop, lower = 0)
  assert_number(gdp, lower = 0)
  assert_number(enInt, lower = 0)
  assert_number(carbInt, lower = 0)
  pop * gdp * enInt * carbInt
}
